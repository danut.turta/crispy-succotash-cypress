module.exports = {
    plugins: [
      'cypress',
      'prettier'
    ],
    rules: {
      'cypress/no-assigning-return-values': 'error',
      'cypress/no-unnecessary-waiting': 'error',
      'cypress/assertion-before-screenshot': 'warn',
      'cypress/no-force': 'warn',
      'cypress/no-async-tests': 'error',
      'cypress/no-pause': 'error',
      'no-unused-vars': ['error', {args: 'none'}],
      'no-console': 'off',
      'semi': ['error', 'always'],
      'no-new-object': 'error',
      'no-var': 'error',
      'no-const-assign': 'error',
      'no-array-constructor': 'error',
      'array-callback-return': 'error',
      'prefer-destructuring': [
        'error',
        {
          VariableDeclarator: {
            array: false,
            object: true
          },
          AssignmentExpression: {
            array: true,
            object: false
          }
        },
        {
          enforceForRenamedProperties: false
        }
      ],
      'no-useless-escape': 'error',
      'space-before-function-paren': [
        'error',
        {
          anonymous: 'never',
          named: 'never',
          asyncArrow: 'always'
        }
      ],
      'space-before-blocks': [
        'error',
        {
          functions: 'always',
          keywords: 'always',
          classes: 'always'
        }
      ],
      'no-param-reassign': 'error',
      'arrow-spacing': ['error', {before: true, after: true}],
      'arrow-parens': ['error', 'as-needed'],
      'arrow-body-style': ['error', 'as-needed'],
      'no-useless-constructor': 'error',
      'no-duplicate-imports': 'error',
      'dot-notation': 'error',
      'no-multi-assign': 'error',
      'eqeqeq': 'error',
      'no-case-declarations': 'error',
      'no-nested-ternary': 'error',
      'no-unneeded-ternary': 'error',
      'space-infix-ops': 'error',
      'no-whitespace-before-property': 'error',
      'comma-spacing': ['error', {before: false, after: true}],
      'computed-property-spacing': ['error', 'never'],
      'func-call-spacing': ['error', 'never'],
      'key-spacing': [
        'error',
        {
          singleLine: {
            beforeColon: false,
            afterColon: true
          },
          multiLine: {
            beforeColon: false,
            afterColon: true
          }
        }
      ],
      'comma-style': ['error', 'last'],
      'object-shorthand': 'error',
      'no-eval': 'error',
      'no-implied-eval': 'error',
      'no-lone-blocks': 'error',
      'no-multi-spaces': 'error',
      'no-redeclare': 'error',
      'no-self-compare': 'error',
      'no-dupe-args': 'error',
      'no-dupe-keys': 'error',
      'no-duplicate-case': 'error',
      'no-empty': 'error',
      'no-unreachable': 'error',
      'no-implicit-coercion': 'error',
      'lines-between-class-members': ['error', 'always'],
      'prefer-template': 'error',
      'no-useless-computed-key': 'error',
      'no-await-in-loop': 'error',
      'no-useless-rename': 'error',
      'no-template-curly-in-string': 'error',
      'block-scoped-var': 'error',
      'no-eq-null': 'error',
      'no-loop-func': 'error',
      'no-return-await': 'error',
      'no-return-assign': ['error', 'always'],
      'no-unmodified-loop-condition': 'error',
      'no-useless-concat': 'error',
      'require-await': 'error',
      'prefer-arrow-callback': 'error',
      'prefer-const': 'error',
      'no-useless-return': 'error',
      'curly': ['error', 'all'],
      'consistent-return': ['error', {treatUndefinedAsUnspecified: true}],
      'no-empty-function': 'error',
      'prettier/prettier': [
        'error',
        {
          singleQuote: true,
          printWidth: 120,
          useTabs: true,
          bracketSpacing: false,
          trailingComma: 'none',
          arrowParens: 'avoid',
          endOfLine: 'auto'
        }
      ]
    },
    extends: [
      'eslint:recommended',
      'plugin:cypress/recommended',
      'plugin:prettier/recommended'
    ],
    parser: '@babel/eslint-parser',
    parserOptions: {
      'ecmaVersion': 6,
      'sourceType': 'module',
      'requireConfigFile': false
    }
}