# Crispy Succotash E2E Framework

#### A framework designed to test end-to-end flows in Cypress for the cripsy-succotash application

## Running the tests

The tests can be run in two ways: locally and with docker-compose

To run the tests you'll first need to:
1. Install Docker
2. Install Node
3. run command:
```shell
# This will install the necessary libraries and adds crispy-succotash repository as a submodule
npm run init-project
```

For running tests locally:
```shell
# This will start the crispy-succotash app
npm run start-services
# This will run the tests using the default browser in headed mode
npm run cy:run
```

For running tests in headless mode use this instead:
```shell
npm run cy:run:headless
```

For targeting other browsers just append `-- --browser <browser>` to the run commands

For cleaning up the test data before re-running the tests you can use:
```shell
npm run resetenv
```

For running tests with docker-compose (NOTE: you need to run start-services beforehand):
```shell
# This will bring up a container with the tests and run them headlessly on Firefox
# Attempted both on Chrome and Electron, but there was a weird issue where the tests would
# just hang right before starting, but after "describe". Unfortunately, I couldn't find the reason for why that happens
# The container will connect to the same network as the crispy-succotash app via the .env file
# I've attempted the same technique in Gitlab CI, but for some reason, the Cypress container would not
# find the crispy-succotash app (https://gitlab.com/danut.turta/crispy-succotash-cypress/-/jobs/1733226165)

npm run cy:run:docker
```

For changing the browser used in the docker-compose run, simply edit `cypress.yml` to use the desired browser instead of `firefox`

### For running tests in Gitlab CI

The tests will start automatically when merging into master, but can also be started on demand starting a pipeline with the following variable:
```shell
RUN_CYPRESS = true
```

### Eslint

Besides the cypress tests, there is also a linter to verify the JS code and apply a prettier on it
It automatically runs on Gitlab CI on every push and can be triggered locally using the following:
```shell
npm run cy:lint
```

and for fixing fixable lint issues:
```shell
npm run cy:lint:fix
```

## Test Strategy

The complete test strategy can be found here: 
https://miro.com/app/board/o9J_lmpuiVw=/?invite_link_id=33438401390


It mainly focuses on the following principles:

- Handle in UI the core user journeys identified as the following
1. A user should be able to register and have access to his account after that
2. A user should be able to login to his account
3. A user should be able to see all feeds from all users
4. A user should be able to go to a feed and subsequent feed entry and add comments to it
5. A user should be able to add a new feed and see it in his feeds
6. A user should be able to bookmark a feed and see it in his bookmarked feeds

- Handle in unit and integration tests most of the other cases. Especially when handling feed entries
that can change and disappear after a few days (because we're cleaning up the database on every run), 
using a mocked RSS feed in units and integration tests
would be the more reliable option. Besides those, there is also the comment editor which would have too
many options to test in Cypress

## Framework

#### The framework is split into two parts

### Tests (integration folder)

Each test handles a different core journey.

Except Register and Login, each test creates a new authenticated session (via the api) with a newly created user
before the beginning of each test. They will also add any needed test data (seeds as Cypress calls them) in the same
`before` section, again via the API

### Sources (src folder)

#### Pages

Each page in the application should have an afferent Page.js file in `pages` folder.
Page.js files should contain functions that clearly describe an action related to that page,
be it getting an element from that page or performing an action on a certain element.
Selectors should be stored in constants that describe what they intend to target and,
when deciding what selector to be used, unique ids or classes should be preferred. When that's
not possible, the element should be selected based on as many selectors as possible to ensure its
uniqueness.

When writing a function for an action on an element, its name should describe it in a natural way of
thinking, rather than a technical way e.g.:

```
// This is not preferred
function setTextInComment(text) {}

// This is preferred
function writeComment(text) {}
```

Also, for getting elements to perform actions on it, the naming should avoid "get" to ensure
fluency when reading them in a test e.g.:

```
// This is not preferred
function getAddedByInput() { return <something>;}
() => {
    getAddedByInput().should('have.text', <someText>);
}

// This is preferred
function addedBy() { return <something>;}
() => {
    addedBy().should('have.text', <someText>);
}
```

#### Seeds

Almost all tests will require some kind of test data beforehand. As with the pages files,
the seed functions should also describe clearly what they intend to add or do.

All requests must be made with csrfmiddletoken that can be extracted from the csrftoken cookie.
An utility function has been created for handling this, that takes a target url and the form body
as parameters and performs a POST request using those and appending the csrfmiddletoken to the body.
All seed functions should go through that utility function


## Issues:

All issues found in the crispy-succotash app have been logged in the Gitlab repository
(https://gitlab.com/danut.turta/crispy-succotash-cypress/-/issues)
