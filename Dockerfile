FROM cypress/browsers:node16.5.0-chrome94-ff93
WORKDIR /

# Copying both the test files and the config for cypress
COPY cypress cypress
COPY cypress.json .
COPY package.json .
COPY package-lock.json .

RUN npm ci