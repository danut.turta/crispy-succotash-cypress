function postTokenizedFormToUrlWithBody(targetedUrl, bodyToPost) {
	cy.request({
		url: targetedUrl,
		method: 'HEAD'
	}).then(() => {
		cy.getCookie('csrftoken')
			.its('value')
			.then(token => {
				cy.request({
					url: targetedUrl,
					method: 'POST',
					form: true,
					followRedirect: false,
					body: {
						...bodyToPost,
						csrfmiddlewaretoken: token
					}
				});
			});
	});
}

function addUserWithCredentials(username, password) {
	postTokenizedFormToUrlWithBody('/accounts/register/', {
		username,
		password1: password,
		password2: password
	});
}

function loginWithCredentials(username, password) {
	postTokenizedFormToUrlWithBody('/accounts/login/', {
		username,
		password
	});
}

function addNewFeed(feedUrl) {
	postTokenizedFormToUrlWithBody('/feeds/new/', {
		feed_url: feedUrl,
		submit: 'Submit'
	});
}

function cleanUpSession() {
	cy.clearLocalStorage();
	cy.clearCookies();
}

export {addUserWithCredentials, cleanUpSession, loginWithCredentials, addNewFeed};
