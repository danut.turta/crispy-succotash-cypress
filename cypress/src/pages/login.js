const usernameSelector = 'input[id="id_username"]';
const passwordSelector = 'input[id="id_password"]';
const loginSelector = 'input[type="submit"]';

function typeUsername(username) {
	cy.get(usernameSelector).type(username);
}

function typePassword(password) {
	cy.get(passwordSelector).type(password);
}

function pressLogin() {
	cy.get(loginSelector).click();
}

function loginWithCredentials(username, password) {
	typeUsername(username);
	typePassword(password);
	pressLogin();
}

export {loginWithCredentials};
