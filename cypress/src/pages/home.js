const accountNavigationOptions = {
	signUp: 'a[href*="register"]',
	login: 'a[href*="login"]',
	logout: 'a[href*="logout"]'
};

const feedNavigationOptions = {
	newFeed: 'a[href*="feeds/new"]',
	myFeeds: 'a[href*="feeds/my"]',
	bookmarked: 'a[href*="feeds/bookmarked"]'
};

function openHomePage() {
	cy.visit('/');
}

function navigateTo(navigationOptionSelector) {
	cy.get(navigationOptionSelector).click();
}

function logoutButton() {
	return cy.get(accountNavigationOptions.logout);
}

export {openHomePage, navigateTo, accountNavigationOptions, feedNavigationOptions, logoutButton};
