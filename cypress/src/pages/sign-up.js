const usernameSelector = 'input[id="id_username"]';
const passwordSelector = 'input[id="id_password1"]';
const confirmationSelector = 'input[id="id_password2"]';
const submitSelector = 'input[id="submit-id-submit"]';

function typeUsername(username) {
	cy.get(usernameSelector).type(username);
}

function typePassword(password) {
	cy.get(passwordSelector).type(password);
}

function typeConfirmation(confirmationPassword) {
	cy.get(confirmationSelector).type(confirmationPassword);
}

function submitRegistrationInformation() {
	cy.get(submitSelector).click();
}

function registerANewUserWithCredentials(username, password) {
	typeUsername(username);
	typePassword(password);
	typeConfirmation(password);
	submitRegistrationInformation();
}

export {typeUsername, typePassword, typeConfirmation, submitRegistrationInformation, registerANewUserWithCredentials};
