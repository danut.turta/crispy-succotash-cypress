const urlInputSelector = 'input[id="id_feed_url"]';
const submitUrlSelector = 'input[id="submit-id-submit"]';

function writeUrl(url) {
	cy.get(urlInputSelector).type(url);
}

function submitUrl() {
	cy.get(submitUrlSelector).click();
}

export {writeUrl, submitUrl};
