const feedDetailsSectionSelector = 'dl.dl-horizontal';
const feedDetailsSectionItemSelector = `${feedDetailsSectionSelector}>dd`;
const feedTitleSelector = 'header>h1';
const feedEntriesListSelector = '.table';
const feedEntrySelector = 'tr';
const feedEntryUrlSelector = 'a';
const bookmarkSelector = '.glyphicon-heart-empty';

const addedByIndex = 0;
const feedUrlIndex = 1;

const latestFeedEntryIndex = 1;

function addedBy() {
	return cy.get(feedDetailsSectionItemSelector).eq(addedByIndex);
}

function feedUrl() {
	return cy.get(feedDetailsSectionItemSelector).eq(feedUrlIndex);
}

function feedTitle() {
	return cy.get(feedTitleSelector);
}

function latestFeedEntry() {
	return cy.get(feedEntriesListSelector).find(feedEntrySelector).eq(latestFeedEntryIndex).find(feedEntryUrlSelector);
}

function viewLatestFeedEntry() {
	latestFeedEntry().click();
}

function bookmarkFeed() {
	cy.get(bookmarkSelector).click();
}

export {addedBy, feedUrl, feedTitle, viewLatestFeedEntry, bookmarkFeed};
