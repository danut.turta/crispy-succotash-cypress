const feedSelector = 'tr';
const feedUrlSelector = 'a';

function feed(feedName) {
	return cy.contains(feedName);
}

function feedWithUrl(feedName, url) {
	return cy.contains(url).parent(feedSelector).find(feedUrlSelector, feedName);
}

function viewFeedWithUrl(feedName, url) {
	feedWithUrl(feedName, url).click();
}

export {feed, feedWithUrl, viewFeedWithUrl};
