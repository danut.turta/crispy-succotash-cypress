const commentEditorSelector = 'textarea';
const submitCommentSelector = 'input[id="submit-id-submit"]';
const commentAuthorSelector = 'div.row>div.col-sm-1';
const commentContentSelector = 'div.col-sm-11';

function writeComment(comment) {
	cy.get(commentEditorSelector).focus().type(comment);
}

function submitComment() {
	cy.get(submitCommentSelector).click();
}

function commentWrittenBy(comment, author) {
	return cy.get(commentAuthorSelector).contains(author).siblings(commentContentSelector).contains(comment);
}

export {writeComment, submitComment, commentWrittenBy};
