import {addUserWithCredentials, loginWithCredentials, cleanUpSession, addNewFeed} from '../src/support/seed';
import {feedWithUrl, viewFeedWithUrl} from '../src/pages/feed-list';
import {viewLatestFeedEntry} from '../src/pages/detailed-feed';
import {writeComment, submitComment, commentWrittenBy} from '../src/pages/detailed-feed-entry';
import {openHomePage} from '../src/pages/home';

describe('An existing user', () => {
	before(() => {
		addUserWithCredentials('Maria', 'securePassword123!');
		cleanUpSession();
		addUserWithCredentials('Mark', 'securePassword123!');
		addNewFeed('https://feeds.feedburner.com/tweakers/mixed');
		cleanUpSession();
		addUserWithCredentials('Hannah', 'securePassword123!');
		addNewFeed('http://feeds.bbci.co.uk/news/world/rss.xml');
	});

	beforeEach(() => {
		loginWithCredentials('Maria', 'securePassword123!');
		openHomePage();
	});

	it('should be able to see feeds added by others', () => {
		feedWithUrl('BBC News - World', 'http://feeds.bbci.co.uk/news/world/rss.xml').should('be.visible');
		feedWithUrl('Tweakers Mixed RSS Feed', 'https://feeds.feedburner.com/tweakers/mixed').should('be.visible');
	});

	it('should be able to navigate to a feed entry and add a comment', () => {
		viewFeedWithUrl('BBC News - World', 'http://feeds.bbci.co.uk/news/world/rss.xml');
		viewLatestFeedEntry();
		writeComment('This is a great news!');
		submitComment();
		commentWrittenBy('This is a great news!', 'Maria').should('be.visible');
	});
});
