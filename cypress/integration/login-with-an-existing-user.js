import {addUserWithCredentials, cleanUpSession} from '../src/support/seed';
import {openHomePage, navigateTo, accountNavigationOptions, logoutButton} from '../src/pages/home';
import {loginWithCredentials} from '../src/pages/login';

describe('An existing user', () => {
	before(() => {
		cleanUpSession();
		addUserWithCredentials('Charlie', 'securePassword123!');
		cleanUpSession();
	});

	it('should be able to log into his account', () => {
		openHomePage();
		navigateTo(accountNavigationOptions.login);
		loginWithCredentials('Charlie', 'securePassword123!');
		logoutButton().should('be.visible');
	});
});
