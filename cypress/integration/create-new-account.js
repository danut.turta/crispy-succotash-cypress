import {openHomePage, accountNavigationOptions, navigateTo, logoutButton} from '../src/pages/home';
import {registerANewUserWithCredentials} from '../src/pages/sign-up';
import {cleanUpSession} from '../src/support/seed';

describe('A new user', () => {
	before(() => {
		cleanUpSession();
	});

	it('should be able to register a new account', () => {
		openHomePage();
		navigateTo(accountNavigationOptions.signUp);
		registerANewUserWithCredentials('Daniel', 'securePassword123!');
		logoutButton().should('be.visible');
	});
});
