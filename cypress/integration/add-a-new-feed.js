import {addUserWithCredentials, cleanUpSession, loginWithCredentials, addNewFeed} from '../src/support/seed';
import {feed} from '../src/pages/feed-list';
import {writeUrl, submitUrl} from '../src/pages/new-feed';
import {feedTitle, addedBy, feedUrl} from '../src/pages/detailed-feed';
import {navigateTo, feedNavigationOptions, openHomePage} from '../src/pages/home';

describe('An existing user', () => {
	before(() => {
		addUserWithCredentials('Jeremy', 'securePassword123!');
		addNewFeed('http://feeds.bbci.co.uk/news/technology/rss.xml?edition=uk');
		cleanUpSession();
		addUserWithCredentials('Karen', 'securePassword123!');
		addNewFeed('http://feeds.bbci.co.uk/news/science_and_environment/rss.xml?edition=uk');
	});

	it('should be able to add a new feed and see it in his feeds', () => {
		loginWithCredentials('Jeremy', 'securePassword123!');
		openHomePage();

		navigateTo(feedNavigationOptions.newFeed);
		writeUrl('https://www.nu.nl/rss/Algemeen');
		submitUrl();

		feedTitle().should('have.text', 'NU - Algemeen');
		addedBy().should('have.text', 'Jeremy');
		feedUrl().should('have.text', 'https://www.nu.nl/rss/Algemeen');

		navigateTo(feedNavigationOptions.myFeeds);
		feed('NU - Algemeen').should('be.visible');
		feed('BBC News - Technology').should('be.visible');
		feed('BBC News - Science & Environment').should('not.exist');
	});
});
