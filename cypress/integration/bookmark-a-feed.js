import {addUserWithCredentials, cleanUpSession, addNewFeed, loginWithCredentials} from '../src/support/seed';
import {feed, viewFeedWithUrl} from '../src/pages/feed-list';
import {navigateTo, feedNavigationOptions, openHomePage} from '../src/pages/home';
import {bookmarkFeed} from '../src/pages/detailed-feed';

describe('An existing user', () => {
	before(() => {
		addUserWithCredentials('Clara', 'securePassword123!');
		cleanUpSession();
		addUserWithCredentials('James', 'securePassowrd123!');
		addNewFeed('https://www.reutersagency.com/feed/?best-sectors=economy&post_type=best');
		cleanUpSession();
	});

	it('should be able to bookmark a feed and see it in his bookmarks', () => {
		loginWithCredentials('Clara', 'securePassword123!');
		openHomePage();
		viewFeedWithUrl(
			'Economy & Policy | Reuters News Agency',
			'https://www.reutersagency.com/feed/?best-sectors=economy&post_type=best'
		);
		bookmarkFeed();
		navigateTo(feedNavigationOptions.bookmarked);
		feed('Economy & Policy | Reuters News Agency').should('be.visible');
	});
});
